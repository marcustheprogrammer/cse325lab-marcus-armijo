ESTIMATE of time to complete lab: 7 hours 

Date	Start Time	Time Spent	Lab Part	Work Completed
____	__________	__________	________	______________
2/1	7:48 pm		00:01				Added Log File	
2/1	8:27 pm		00:12		1		Downloaded AMD64 and Intel Manuals and read exercise 1
2/1	8:37 pm		00:26		2		Read exercise 2 and ran through steps
2/1	8:54 pm		00:08		3		Read exercise 3 attempted stepping through with gdb
2/1	9:02 pm		00:38		4		Used GDB to trace through breakpoint at 0x7c00 and compared to files boot.S and boot.ASM traced through other points using GDB as well
2/1	9:40 pm		00:21		5		Read through exercise 5 and ran object dump commands looked up ELF format
2/1	10:02 pm	00:05		6		Read through and broke the link address from 0x7c00 to 0x7c01 got "triple fault" as an error. Corrected. Rebuilt ran object dump
2/1	10:07 pm	00:20		7		Read through 7 pretty sort section about alternative booting platforms also explained x gdb command
2/1	10:28 pm	00:15		7		Read the rest of 7 after the start of Part 3
2/2	7:18 pm		00:00		8		Read 8 forgot fixing log file
2/2	7:18 pm		00:04		9		Wrote code for octal printing exercise 9
2/2	7:24 pm		00:10		10 		Read through exercise 10 a couple times
2/2	7:32 pm		00:20		11		Read through exercise 11 as well as C files
2/3	7:56 pm		03:00		12 13		writing mon_backtrace, reading, trying to understand current code still getting FAIL for backtrace count
			_____
			06:00	TOTAL time spent

The hardest part was trying to undestand what the provided functions were doing. I spent a majority of the time writing
the mon_backtrace(). I could not get the First test in the grader to work the whole way. I got 7 out of 8 I think it has
something to do with the first one being zero. I ran of time though to fix it. The information was a little overwhelming to
be in one packet. 
